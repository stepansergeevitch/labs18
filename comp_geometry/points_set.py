import functools
from itertools import chain
from bintrees import RBTree

from PyQt5 import QtGui, QtCore, Qt
from PyQt5.QtWidgets import QWidget

from utils import *
# from pytess import voronoi

class DebugVector:
    def __init__(self, begin, end):
        self.begin = (begin.x, begin.y)
        self.end = (end.x, end.y)
        self.point_radius = 2

    def render_self(self, painter):
        brush = painter.brush()
        painter.setBrush(Qt.QBrush(Qt.QColor(255, 0, 0)))
        painter.drawLine(*self.begin, *self.end)
        painter.drawEllipse(QtCore.QRectF(
                self.end[0] - (self.point_radius), 
                self.end[1] - (self.point_radius),
                self.point_radius * 2,
                self.point_radius * 2
            ))
        painter.setBrush(brush)

class DebugPoint:
    def __init__(self, point):
        self.point = point
        self.point_radius = 2

    def render_self(self, painter):
        brush = painter.brush()
        painter.setBrush(Qt.QBrush(Qt.QColor(255, 0, 0)))
        painter.drawEllipse(QtCore.QRectF(
                self.point[0] - (self.point_radius), 
                self.point[1] - (self.point_radius),
                self.point_radius * 2,
                self.point_radius * 2
            ))
        painter.setBrush(brush)

class DebugCircle:
    def __init__(self, center, radius):
        self.center, self.radius = center, radius

    def render_self(self, painter):
        brush = painter.brush()
        painter.setBrush(Qt.QBrush(Qt.QColor(255, 0, 0)))
        painter.drawArc(QtCore.QRectF(
                self.center[0] - (self.radius), 
                self.center[1] - (self.radius),
                self.radius * 2,
                self.radius * 2
            ), 0, 16 * 360)
        painter.setBrush(brush)




class PointsSet:
    def __init__(self, points=[]):
        self.points = points
        self.circle_center = (0, 0)
        self.circle_radius = 0
        self.recalculate_convex_hull()
        self.debug_items = []

   
    def recalculate_state(self, render=True):
        self.debug_items = []
        self.recalculate_convex_hull()
        self.recalculate_inscribed_circle()
        if render:
            self.render()

    def render(self):
        if self.renderer:
            self.renderer.update()
        else:
            print('Warning: no renderer set')

    def add_point(self, point):
        self.points.append(point)
        self.recalculate_state()

    def compare_points(self, point1, point2):
        if point1[0] == point2[0]:
            return point1[1] - point2[1]
        return point1[0] - point2[0]

    def split_array_by_cond(self, array, cond):
        arr_true, arr_false = [], []
        for item in array:
            cond_val = cond(item)
            if cond_val >= 0:
                arr_true.append(item)
            if cond_val <= 0:
                arr_false.append(item)
        return arr_true, arr_false

    def above_line_condition(self, point1, point2):
        def inner(point):
            return (point[0] - point1[0]) * (point2[1] - point1[1]) -\
                   (point[1] - point1[1]) * (point2[0] - point1[0]) 
        return inner

    def check_rotation(self, p1, p2, p3, right):
        value = (p2[0] - p1[0]) * (p3[1] - p1[1]) -\
                (p2[1] - p1[1]) * (p3[0] - p1[0]) 
        if (value > 0 and right) or (value < 0 and not right):
            return True
        return False

    def build_hull(self, points, upper):
        if len(points) <= 2:
            return list(points)
        res = points[:2]
        for point in points[2:]:
            while len(res) > 1 and not self.check_rotation(res[-2], res[-1], point, upper):
                res.pop()
            res.append(point)
        return res


    def recalculate_convex_hull(self):
        self.convex_hull = []
        if len(self.points) < 2:
            self.convex_hull = list(self.points)
            return
        # self.points.sort(key=functools.cmp_to_key(self.compare_points))
        self.points = sorted(self.points, key=functools.cmp_to_key(self.compare_points))
        left_point, right_point = self.points[0], self.points[-1]
        upper, lower = self.split_array_by_cond(
            self.points,
            self.above_line_condition(left_point, right_point)
        )
        upper_hull, lower_hull = self.build_hull(upper, True), self.build_hull(lower, False)
        self.convex_hull = upper_hull + lower_hull[1:-1][::-1] 


    def __iter__(self):
        return self.points.__iter__()

    def iter_hull(self):
        ch_len = len(self.convex_hull)
        for i in range(ch_len):
            yield self.convex_hull[i], self.convex_hull[(i + 1) % ch_len]
        raise StopIteration()

    def iter_hull_with_neighbours(self):
        ch_len = len(self.convex_hull)
        for i in range(ch_len):
            yield (
                self.convex_hull[(i - 1) % ch_len],
                self.convex_hull[i],
                self.convex_hull[(i + 1) % ch_len],
                self.convex_hull[(i + 2) % ch_len]
            ), i
        raise StopIteration()

    # Circle

    def get_collapse_times_set(self):
        return RBTree(list(
            (i, get_collapse_time(
                self.line_at(i - 1),
                self.line_at(i),
                self.line_at(i + 1)
            ))
            for i in range(len(self.convex_hull))
        ))

    def line_at(self, i):
        return (
            self.convex_hull[i % len(self.convex_hull)],
            self.convex_hull[(i + 1)% len(self.convex_hull)]
        )

    def recalculate_inscribed_circle(self):
        # print(get_collapse_point(
        #     ((0, -10), (-5, 0)),
        #     ((-5, 0), (0, 10)),
        #     ((0, 10), (10, 10)),
        #     self
        # ))
        # return


        if len(self.convex_hull) < 3:
            self.circle_radius = 0
            self.circle_center = (0, 0)
            return


        collapse_times = self.get_collapse_times_set()

        next_ind = list(
            (i + 1) % len(self.convex_hull)
            for i in range(len(self.convex_hull))
        )
        prev_ind = list(
            (i - 1) % len(self.convex_hull)
            for i in range(len(self.convex_hull))
        )
     
        time, ind = None, None
        while (len(collapse_times) > 3):
            ind, time = collapse_times.pop_min()
     
            nxt = next_ind[ind]
            prv = prev_ind[ind]
            next_ind[prv] = nxt
            prev_ind[nxt] = prv
            if lines_parallel(
                       self.line_at(nxt),
                       self.line_at(prv)
                    ):
                break
     
            tmp1 = collapse_times.pop(prv)
            tmp2 = collapse_times.pop(nxt)

            collapse_times.insert(
                prv,
                get_collapse_time(
                    self.line_at(prev_ind[prv]),
                    self.line_at(prv),
                    self.line_at(next_ind[prv])
                )
            )

            collapse_times.insert(
                nxt,
                get_collapse_time(
                    self.line_at(prev_ind[nxt]),
                    self.line_at(nxt),
                    self.line_at(next_ind[nxt])
                )
            )
        ind, time = collapse_times.pop_min()

        for ind in range(len(self.convex_hull)):
            center = get_collapse_point(
                self.line_at(ind - 1),
                self.line_at(ind),
                self.line_at(ind + 1),
                self
            )

        if ind is not None:
            self.circle_center = get_collapse_point(
                self.line_at(ind - 1),
                self.line_at(ind),
                self.line_at(ind + 1),
                self
            )
            # self.debug_point(self.circle_center)
            self.circle_radius = get_collapse_time(
                self.line_at(ind - 1),
                self.line_at(ind),
                self.line_at(ind + 1),
            )
        else:
            self.circle_center = (0, 0)
        # self.circle_radius = time or 0

    def recalculate_inscribed_circle_2(self):
        if len(self.convex_hull) < 3:
            self.circle_radius = 0
            self.circle_center = (0, 0)
            return

        v = voronoi(self.convex_hull)
        for p in chain.from_iterable(p[1] for p in v):
            self.debug_point(p)

    def debug_point(self, point):
        self.debug_items.append(DebugPoint(point))

    def debug_vector(self, begin, end):
        self.debug_items.append(DebugVector(begin, end))

    def debug_circle(self, center, radius):
        self.debug_items.append(DebugCircle(center, radius))        
