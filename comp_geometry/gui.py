from PyQt5 import QtGui, QtCore, Qt
from PyQt5.QtWidgets import QWidget



class PointsRenderer(QWidget):
    def __init__(self, parent = None):
    
        QWidget.__init__(self, parent)
        self.points_set = None
        self.mouseReleaseEvent = self.handle_click
        self.paintEvent = self.render
        self.point_radius = 2

    def link_points_set(self, points_set):
        self.points_set = points_set
        points_set.renderer = self

    def handle_click(self, event):
        self.points_set.add_point((event.x(), event.y()))

    def get_circle_rect(self):
        return QtCore.QRectF(
            self.points_set.circle_center[0] - self.points_set.circle_radius,
            self.points_set.circle_center[1] - self.points_set.circle_radius,
            2 * self.points_set.circle_radius,
            2 * self.points_set.circle_radius
        )

    def render(self, event):
        painter = QtGui.QPainter(self)
        painter.setPen(Qt.QPen(Qt.QColor(1, 0, 0)))
        painter.setBrush(Qt.QBrush(Qt.QColor(0, 0, 0)))
        for point in self.points_set:
            painter.drawEllipse(QtCore.QRectF(
                point[0] - (self.point_radius), 
                point[1] - (self.point_radius),
                self.point_radius * 2,
                self.point_radius * 2
            ))
        for line in self.points_set.iter_hull():
            painter.drawLine(*line[0], *line[1])

        painter.drawArc(self.get_circle_rect(), 0, 16 * 360)
        self.render_debugs(painter)

    def render_debugs(self, painter):
        for item in self.points_set.debug_items:
            item.render_self(painter)
