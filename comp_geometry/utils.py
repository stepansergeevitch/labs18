from math import sqrt, atan2, pi, sin

class Point:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def __sub__(self, other):
        return Point(self.x - other.x, self.y - other.y)

    def shift(self, vector):
        self.x += vector.x
        self.y += vector.y


class Line:
    def __init__(self, p1, p2):
        self.a = p1.y - p2.y
        self.b = p2.x - p1.x
        self.c = -self.a * p1.x - self.b * p1.y
        length = (self.a ** 2 + self.b ** 2)
        self.a /= length
        self.b /= length
        self.c /= length

class Vector:
    def __init__(self, x, y):
        self.length = sqrt(x ** 2 + y ** 2)
        self.x = x
        self.y = y

    def set_length(self, length):
        self.x *= length / self.length
        self.y *= length / self.length
        self.length = length


def dist(p1, p2):
    return sqrt((p1.x - p2.x) ** 2 + (p1.y - p2.y) ** 2)


def get_ang(p1, p2):
    ang = abs(atan2(p1.y, p1.x) - atan2(p2.y, p2.x))
    return min(ang, 2 * pi - ang)


def det(a, b, c, d):
    return a * d  - b * c

def intersect(l1, l2):
    zn = det(l1.a, l1.b, l2.a, l2.b)
    return Point(
        - det(l1.c, l1.b, l2.c, l2.b) / zn,
        - det(l1.a, l1.c, l2.a, l2.c) / zn
    )

def parallel(p1, p2):
    eps = 1e-9
    return abs(det(p1.a, p1.b, p2.a, p2.b)) < eps

def get_h(p1, p2, l1, l2, r1, r2):
    q1 = intersect(Line(p1, p2), Line(l1, l2))
    q2 = intersect(Line(p1, p2), Line(r1, r2))
    l = dist(q1, q2)
    alpha = get_ang(l2 - l1, p2 - p1) / 2
    beta = get_ang(r2 - r1, p1 - p2) / 2
    return l * sin(alpha) * sin(beta) / sin(alpha + beta)

def shift(p, v):
    return Point(p.x + v.x, p.y + v.y)

def get_point(p1, p2, l1, l2, r1, r2, parent):
    q1 = intersect(Line(p1, p2), Line(l1, l2))
    q2 = intersect(Line(p1, p2), Line(r1, r2))
    l = dist(q1, q2)
    alpha = get_ang(l2 - l1, p2 - p1) / 2
    beta = get_ang(r2 - r1, p1 - p2) / 2
    sa = sin(alpha)
    sb = sin(beta)
    h = l * sa * sb / sin(alpha + beta)
    shift_vector = Vector(p1.y - p2.y, p2.x - p1.x)
    shift_vector.set_length(h)
    middle = Point(
        (q1.x * sa + q2.x * sb) / (sa + sb),
        (q1.y * sa + q2.y * sb) / (sa + sb),
    )
    # print(alpha * (180 / pi), beta * (180 / pi))
    # print(h)
    P = shift(middle, shift_vector)

    # parent.debug_vector(middle, P)
    # parent.debug_vector(q1, P)
    # parent.debug_vector(q2, P)

    return P


# Interface

def get_collapse_time(line_left, line, line_right):
    args = [Point(*i) for i in (line + line_left[::-1] + line_right)]
    return get_h(*args)

def get_collapse_point(line_left, line, line_right, parent=None):
    args = [Point(*i) for i in (line + line_left[::-1] + line_right)]
    res = get_point(*args, parent)
    return (res.x, res.y)

def intersect_lines(line1, line2):
    res = intersect(
        Line(Point(*line1[0]), Point(*line1[1])),
        Line(Point(*line2[0]), Point(*line2[1]))
    )
    return (res.x, res.y)

def lines_parallel(line1, line2):
    return parallel(
        Line(Point(*line1[0]), Point(*line1[1])),
        Line(Point(*line2[0]), Point(*line2[1]))
    )

def points_angle(p1, p2, p3):
    return get_ang(Point(*p2) - Point(*p1), Point(*p3) - Point(*p2))


