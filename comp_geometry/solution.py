import sys
from PyQt5.QtWidgets import QApplication

from gui import PointsRenderer
from points_set import PointsSet

def show_main_window():
    app = QApplication(sys.argv)

    renderer = PointsRenderer()
    points_set = PointsSet()
    renderer.link_points_set(points_set)
    renderer.resize(600, 400)
    renderer.move(200, 100)
    renderer.setWindowTitle('Interactive solution')
    renderer.show()
    
    sys.exit(app.exec_())

if __name__ == '__main__':
	show_main_window()