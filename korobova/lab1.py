import math
import numpy as np
from scipy.misc import derivative
from scipy.optimize import minimize

n = 4
p = np.array([20, 25, 10, 15])
h0 = np.zeros(n)
 
INF = 2000000 
I = 10000
 
def U(x1, x2, x3, x4):
    return 12.3 * math.log2 (x1) + 10.3 * math.log2 (x2 - 6) + 7.8 * math.log2 (x3 - 7) + 3.5 * math.log2 (x4 - 12)
 
def partial_derivative(func, var=0, point=[]):
    args = point[:]
 
    def wraps(x):
        args[var] = x
        return func(*args)
 
    return derivative(wraps, point[var], dx = 1e-6)

x0 = np.array([
    33.542182111332473,
    31.002924221720034,
    48.75634535455467,
    43.434653245650891
])

bnds = [(25, INF)] * n

objective = lambda x: -U(x[0], x[1], x[2], x[3])
constraint = lambda x: I - x[0] * p[0] - x[1] * p[1] - x[2] * p[2] - x[3] * p[3]

x = minimize(
    objective, x0,
    method='SLSQP',
    bounds=bnds,
    constraints=[{
        'type': 'ineq',
        'fun': constraint
    }]
).x

L = [partial_derivative(U, i, x) / val for i, val in enumerate(p)]

print('Final Objective: ' + str(-objective(x)))
print('Solution:\n' + '\n'.join(
    'x{} = {}'.format(i + 1, val) for i, val in enumerate(x)
))

print("\nLagrange multiplier:\n" + '\n'.join(
    'L{} = {}'.format(i + 1, L[i]) for i, val in enumerate(L)
))