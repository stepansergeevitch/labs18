import numpy as np
from scipy.misc import derivative
import math
 
p = [20, 25, 10, 15]
 
n = 4
x0 = np.zeros(n)
h0 = np.zeros(n)
 
INF = 2000000
 
I = 10000
 
def U(x1, x2, x3, x4):
    return 12.3 * math.log2 (x1) + 10.3 * math.log2 (x2 - 6) + 7.8 * math.log2 (x3 - 7) + 3.5 * math.log2 (x4 - 12)
 
 
def partial_derivative(func, var=0, point=[]):
    args = point[:]
 
    def wraps(x):
        args[var] = x
        return func(*args)
 
    return derivative(wraps, point[var], dx = 1e-6)
 
from scipy.optimize import minimize
 
 
def objective(x):
    return - U(x[0], x[1], x[2], x[3])
 
 
def constraint1(x):
    return I - x[0] * p[0] - x[1] * p[1] - x[2] * p[2] - x[3] * p[3]
 
 
x0[0] = 33.542182111332473
x0[1] = 31.002924221720034
x0[2] = 48.75634535455467
x0[3] = 43.434653245650891
 
bnds = (
    (25, INF),
    (25, INF),
    (25, INF),
    (25, INF)
)
 
con1 = {'type': 'ineq', 'fun': constraint1}
cons = ([con1])
solution = minimize(objective, x0, method='SLSQP', bounds=bnds, constraints=cons)
 
x = solution.x
 
print('Final Objective: ' + str(-objective(x)))
 
print('\nSolution')
for i in range(4):
    print('x{} = {}'.format(i + 1, x[i]))
 
print("\nLagrange multiplier")
L = [partial_derivative(U, i, x) / p[i] for i in range(4)]
for i in range(4):
    print('L{} = {}'.format(i + 1, L[i]))