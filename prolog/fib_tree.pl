:- initialization(main).


fibonaci_tree(1, '').
fibonaci_tree(2, '()()').
fibonaci_tree(F, S) :-
    FL is F - 2,
    FR is F - 1,
    fibonaci_tree(FL, SL),
    fibonaci_tree(FR, SR),
    string_concat("(", SL, LL),
    string_concat(LL, ")", L),
    string_concat("(", SR, RL),
    string_concat(RL, ")", R),
    string_concat(R, L,  S).
    
main :- fibonaci_tree(4, A),
    string_concat("(", A, B),
    string_concat(B, ")", C),
    write(C).