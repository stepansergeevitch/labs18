:- initialization(main).

split_list(X, 0, [], X).
split_list([H|T], K, [H|F], S) :-
    L is K - 1,
    split_list(T, L, F, S).

main :- split_list([1, 2, 3, 4], 3, A, B),
    write(A),
    write(B).